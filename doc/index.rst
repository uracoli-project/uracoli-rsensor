.. rsensor documentation master file, created by
   sphinx-quickstart on Sat Oct 27 09:05:23 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to rsensor's documentation!
===================================

The uracoli-rsensor project stores sensor data, received from a MQTT broker in a database.

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   system
   usage
   config
   mqtt
   databases
   usecases
   raspi
   development
   license

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
