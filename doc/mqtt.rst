MQTT Interface
==============

Info Topic rsensor/node/+/info[r]
---------------------------------

The =node_info= topuc is a retained topic that contains information about the sensors 
and actors that it contains. An example of the JSON-data is given here: 

.. code-block:: JSON

	{
	    "actors": [], 
	    "node": {
	        "id": "51fe4a00", 
	        "name": "thlog-51fe4a00", 
	        "board": "esp8266+bm280", 
	        "firmware": "TempHumLogger.py", 
	        "version": "1.02"
	        "geoloc": [ 1, 2], 
	        "loctag": "WH.KG.K2", 
	    }, 
	    "sensors": [
	        {
	            "id": "temp", 
	            "name": "temp", 
	            "type": "float", 
	            "unit": "\u00b0C"
	        }, 
	        {
	            "id": "rh", 
	            "name": "rh", 
	            "type": "float", 
	            "unit": "%"
	        }	    ]
	}

Data Structure
^^^^^^^^^^^^^^

The data in the info topic is a dictionary with three keywords:

.. code-block:: JSON

	{
		"node": {<node_description>}
		"sensors": [{<sensor_description>}, {...}, ...]
		"actors": [{<actor_description>}, {}, ...]
	}

Node Information <node_description>
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

========== =================================================    
 name        description	
========== =================================================    
id		    unique id of the sensor node
name        symbolic name      
board       type of the board                             
firmware    name of the firmware image                     
version		version of the firmware image                                     
loctag      location description                              
geoloc      geographic location of the node (optional) 
========== =================================================    



Sensor Information <sensor_description>
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

========== =================================================    
 name        description	
========== =================================================
id         identifier of the sensor, e.g. 'temp'
name       long name of the sensor, e.g. 'Temperature'
type       data type, e.g. float, int, bool, ...
unit       name of the measurment unit
========== =================================================


Actor Information <actor_description>
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

========== =================================================    
 name        description	
========== =================================================
id         identifier of the sensor, e.g. 'coil1'
...        ...
========== =================================================


Data Topic rsensor/node/+/sensor/data
-------------------------------------

In this topic data from sensors are published.


.. code-block:: JSON

	{ '<sensor_id>' : value,
	  '<sensor_id1>' : value
	}

.. note::

	A published sensor_data message  need not to contain all data defined in
	node_info.sensors. This is because of, that the sensors might have different 
	update rates. 
