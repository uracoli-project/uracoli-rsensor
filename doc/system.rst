Introduction
============

The script ``mqtt_to_db.py`` reads sensor data from one ore more MQTT brokers and stores it together with additional meta 
information in a database.

.. uml::

   @startuml
	
	skinparam componentStyle uml2
	rectangle mqtt {
		[mqtt-BROKER1] #Yellow
		[mqtt-BROKERn] #Yellow		
	}

	rectangle  {
		
		database DataBase #Magenta
		
	}
	file CFG.YML
	
	CFG.YML --> [mqtt_to_db.py]
	
	[mqtt-BROKER1] -right- MQTT
	[mqtt-BROKERn] -right- MQTT
	
	[mqtt-BROKERn] .down. [mqtt-BROKER1]:1...n
	 
	MQTT -right-> [mqtt_to_db.py]

	[mqtt_to_db.py] -> DB
	
	DB - [DataBase]
   @enduml
   
Sensor values are characterized by their meaning, e.g. the physical quantity a value represents.
Beside their basic meaning (numeric value and physical unit), a sensor value can also have meta information attached.

The following information shall be stored in the database:

* What was measured? (numeric value and physical unit)
* When? (the timestamp of the measurement)
* Where? (the location of the sensor)
* How? (the type of the sensor, system information, software version, ...)

