Development Information
=======================

Makefile
--------

The important command lines for development are collected in the `Makefile`.

Docker Environment
------------------

The rsensor-Docker-Container contains the following components:

* Mysql Server
	* the database is initialized with the script ``test/init_db.sql``
* sqlite3
* Python
* the latest rsensor package (todo: really needed?)
* (todo: maybe add mosquitto)


The Container is created with the command::

	cd test
	docker build -t rsensor .
	
The container is started with::

	cd test
	docker run --rm --name rsensordb --hostname rsensordb -v $(pwd):/test -p 5555:5000 -p 3306:3306 -t -i rsensor
	
Options:

:--rm: delete container at exit
:--name ....: set container name
:--hostname ....: set hostname of the container
:-v ...: map local directory as volume
:-p ...: map ports to host system
:-t: ???
:-i: ???
:rsensor: name of the container (see docker build)

At the docker-prompt you can testwise access the database::

	mysql -u root
	
The script ``prepare_mysql.sql`` creates the user rsensor and the database rsensor::

	mysql -u root < prepare_mysql.sql

Using the MySQL Docker DB from extern
-------------------------------------

Acess the container via its IP address with::

	mysql -u rsensor -h 172.17.0.1  rsensor -p 

The password is "rsensor" (see prepare_mysql.sql)

.. todo:: check how to add docker-container to DNS automatically 




 	

