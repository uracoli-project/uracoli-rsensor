.. _config-file:

Config File Format
==================

Format
------

The config file is written in `YAML <https://en.wikipedia.org/wiki/YAML>`_ syntax.

The file contains a YAML-dictionary with two predefined keywords:

- mqtt
- database


MQTT Section
------------

This section starts with the YAML-key ``mqtt`` and contains a list of MQTT-Brokers::

	mqtt:
	 - host: iot.eclipse.org
	   port: 1883
	   prefix: 'clt2020/thlog'
	   info_topic: 'clt2020/thlog/+/info'
	   data_topic: 'clt2020/thlog/+/data'
	 - host: test.mosquitto.org
	   port: 1883
	   prefix: 'aw/thlog'
	   info_topic: 'aw/thlog/+/info'
	   data_topic: 'aw/thlog/+/data'

Each configured MQTT broker requires a YAML structure with the following keys:

:host: hostname or IP address of the broker
:port: portnumber of the mqtt service (1883 unencrypted, 3883 TLS secured)
:prefix: the subscritpion prefix (mqtt path to device level)
:info_topic: the topic where the nodes publish their description information
             per default (if not given) `<prefix>/+/info` is used.
:data_topic:  the topic where the nodes publish their sensor data
             per default (if not given) `<prefix>/+/data` is used.


.. rubric:: Example


::
	myprefix/mydevice/info
	myprefix/mydevice/data
	<prefix>: myprefix
	info_topic: +/info
	data_topic: +/data
	

Database section
----------------

This section starts with the YAML-key ``database`` and requires the ``dbtype`` key::

	database:
	    dbtype: {sqlite|mysql}
	   
The rest of the data-structure depends on the database type used.

SQLITE Database
^^^^^^^^^^^^^^^

The SQLITE DB is configured as follows::

	database:
	    dbtype: sqlite
	    dbname:  /tmp/rsensor/rsensor.sqlitedb

:dbtype: `sqlite` 
:dbname: the path and name of the SQLITE-file


If ``dbname`` refers to a file that not exists, 
the file, including a none existing directory, is created and initialized as sqlite database.

The data in the sqlite database can be accessed with the command::

	sqlite3  /tmp/rsensor/rsensor.sqlitedb 
	sqlite> .tables
	locations   nodes       sensors     timeseries

	
More details about how to work with this DB refer to section :ref:`sqlite-commands`.


MySQL Database
^^^^^^^^^^^^^^

A MySQL Database is configured as follows::

	database:
	    dbtype: mysql
	    dbname: rsensordb
	    dbuser: rsensorusr
	    dbpasswd: topsecret
	    dbhost: database-server.mydomain.com
    

:dbtype: `mysql`
:dbname: database name on the server
:dbuser: database user name that has write access to the database
:dbpasswd: password for database user
:dbhost: server name that hosts the database (hostname, fqdn or IP number)


The database `dbname` must exist and the the `dbuser` must have access. If the database
is empty, all required tables are created by the script.

The created tables can be listed with the command::

	mysql -h <dbhost> -u <dbuser> -p <dbname>
	Enter password: <dbpasswd>
	mysql> show tables;
	+-------------------+
	| Tables_in_rsensor |
	+-------------------+
	| locations         |
	| nodes             |
	| sensors           |
	| timeseries        |
	+-------------------+
	4 rows in set (0,00 sec)
