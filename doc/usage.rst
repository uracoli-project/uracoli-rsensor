Usage
=====

Installation
------------

The installation of the latest version can be done with::

	pip install git+https://gitlab.com/uracoli-project/uracoli-rsensor.git

Getting Started
---------------

After successfull installation run::

	mqtt_to_db -h 
	
to see the available command line options of the script.


Create now the initial configuration file ``myrsensor.cfg``::

	mqtt:
	 - host: test.mosquitto.org
	   port: 1883
	   prefix: rsensor-test
	
	database:
	    dbtype: sqlite
	    dbname:  /tmp/rsensor/rsensor.sqlitedb

This configuration connects to the public available MQTT broker ``test.mosquitto.org`` 
and stores the received data in a SQlite database file ``/tmp/rsensor/rsensor.sqlitedb``.

Run the script and see the messages on the console::

	mqtt_to_db -C myrsensor.cfg
	

The file ``/tmp/rsensor/rsensor.sqlitedb`` is created and can be accessed with the SQLite tool::

	sqlite3 /tmp/rsensor/rsensor.sqlitedb

The next section describes how to receive test data from MQTT, that are stored in the database 

Generating Test Data
--------------------

The package contains also a script ``rs_testgen`` that generates appropriate formatted test data.

The script uses per default the MQTT broker on ``localhost:1883``. 

After start you will see an output::

	$ rs_testgen -H test.mosquitto.org
	INFO:MqttRandomGenerator:create generator class
	using default sensor config: /opt/esp/ve_esp/local/lib/python2.7/site-packages/rsensor/data/test_node.yml
	INFO:MqttRandomGenerator:connected to test.mosquitto.org:1883, prefix=rsensor-test
	INFO:MqttRandomGenerator:info message on: rsensor-test/s_1111_0001/info
	INFO:MqttRandomGenerator:info message on: rsensor-test/s_1111_0002/info
	INFO:MqttRandomGenerator:info message on: rsensor-test/s_1111_0003b/info
	INFO:MqttRandomGenerator:data message on: rsensor-test/s_1111_0001/data
	INFO:MqttRandomGenerator:data message on: rsensor-test/s_1111_0002/data
	INFO:MqttRandomGenerator:data message on: rsensor-test/s_1111_0002/data
	INFO:MqttRandomGenerator:data message on: rsensor-test/s_1111_0003b/data
	INFO:MqttRandomGenerator:data message on: rsensor-test/s_1111_0001/data
	INFO:MqttRandomGenerator:data message on: rsensor-test/s_1111_0002/data

With the command ``mosquitto_sub -h localhost -t "rsensor-test/#" -v`` you can watch 
the generated messages.


See ``rs_testgen -h`` for available configuration options.
 
