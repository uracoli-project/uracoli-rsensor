/*
 'node' : {
        'id': 's_1111_0002',
        'geoloc': [1, 2],
        'loctag': 'Boden',
        'board': 'esp32',
        'firmware' : 'rsensor.hex',
        'version'  : '1.00'
 },
 */
 CREATE TABLE IF NOT EXISTS `nodes`
(
    `node_idx` INTEGER PRIMARY KEY,
    `id` VARCHAR(64) NOT NULL,
    `geoloaction` VARCHAR(64) DEFAULT NULL,
    `loctag` VARCHAR(64) DEFAULT NULL,
    `board` VARCHAR(64) DEFAULT NULL,
    `firmware` VARCHAR(64) DEFAULT NULL,
    `version` VARCHAR(64) DEFAULT NULL
)