DROP DATABASE IF EXISTS rsensor ;
CREATE DATABASE rsensor;
GRANT ALL PRIVILEGES ON rsensor.* TO 'rsensor'@'localhost' IDENTIFIED BY 'rsensor';
GRANT ALL PRIVILEGES ON rsensor.* TO 'rsensor'@'%' IDENTIFIED BY 'rsensor';
FLUSH PRIVILEGES;
select 'Databases:' as '';
show databases;
select 'rsensor tables:' as '';
use rsensor;
show tables;
