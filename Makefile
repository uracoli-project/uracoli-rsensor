VERSION=$(shell python -c "from rsensor import __version__ as v; print v")
cname=rsensor
acwd=$(shell pwd)/test
rcwd=$(notdir $(acwd))


.PHONY: all clean package doc test

#:  build all target
all: test doc package
	@echo BUILD for uracoli-rsensor v$(VERSION) OK

#: clean working directory
clean:
	-rm -vrf dist/* docs/dbdoku.png doc/_build/*
	-docker rm $(shell docker ps -aq)
	-docker rmi $(shell docker images -f "dangling=true" -q)

# === packaging ================================================================
#: Build the python package
package: dist/uracoli-rsensor-$(VERSION).zip

dist/uracoli-rsensor-$(VERSION).zip:
	python setup.py sdist --format=zip

# === documentation ============================================================
# build sphinx documentation
doc:
	make -C doc html

# === test =====================================================================
# run unittests
test:
	pytest -v test

# --- obsolete stuff -----------------------------------------------------------
docx: docs/plantuml.jar docs/dbdoku.png

docs/plantuml.jar:
	wget -O $@ http://sourceforge.net/projects/plantuml/files/plantuml.jar

docs/dbdoku.png: docs/dbdoku.uml
	java -jar docs/plantuml.jar $<

# === pypi handling ============================================================
upload:
	twine register -r pypitest -u uracolix src/dist/uracoli-rsensor-$(VERSION).zip
	twine upload -r pypitest -u uracolix src/dist/uracoli-rsensor-$(VERSION).zip

# === test rukes ===============================================================
drun: package
	rm -rfv test/uracoli-rsensor-*.zip
	cp dist/uracoli-rsensor-$(VERSION).zip test
	#cp src/rsensor_schema.sql test
	cd test &&\
        docker run -v $(acwd):/$(rcwd) -p 5555:5000 -p 3306:3306 -t -i $(cname)

##runserial: package
##	docker run  --device=/dev/ttyUSB0 -v $(acwd):/$(rcwd) -p 5555:5000 -t -i $(cname)
##
##

dbuild:
	cd test && docker build -t $(cname) .

