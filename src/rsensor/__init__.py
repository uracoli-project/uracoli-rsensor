##
#   Copyright (c) 2014 - 2017 Axel Wachtler
#   All rights reserved.
#
#   Redistribution and use in source and binary forms, with or without
#   modification, are permitted provided that the following conditions
#   are met:
#
#   * Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the authors nor the names of its contributors
#     may be used to endorse or promote products derived from this software
#     without specific prior written permission.
#
#   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
#   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
#   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
#   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
#   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
#   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#   POSSIBILITY OF SUCH DAMAGE.

# === imports ==================================================================
import db
from ConfigParser import RawConfigParser
import sys, os, time, pprint

# === globals ==================================================================
VERSION = "0.1.1"
VERBOSE = 0
OLD_STDOUT = None
NEW_STDOUT = None
APP = None

# === functions ================================================================

##
# read config file and merge in parameters
# @param cfgfile
#        name of the win.ini style config file.
# @param sections
#        sections to read from the config file
# @param dfl_parms
#        application default parameters
# @param cmd_parms
#        command line options override config file values
#        and default parameters (dfl_params)
#
# @return dictionary with data
#
def read_and_merge_cfg_file(cfgfile, sections, dfl_params = {}, cmd_params = {}):
    # load default application parameters
    rv = {}
    rv.update(dfl_params)

    # process config parameters
    cfg = RawConfigParser()
    cfg.read(cfgfile)
    for s in sections:
        if cfg.has_section(s):
            d = dict(cfg.items(s))
            rv.update(d)

    #override with command line settings
    rv.update(cmd_params)
    return rv

def exit_on_error(err, diag):
    print >>sys.stderr, "err %d: %s" % (err, diag)
    sys.exit(err)

def redirect_stdout_start(fn = None):
    global OLD_STDOUT, NEW_STDOUT
    if fn:
        OLD_STDOUT = sys.stdout
        NEW_STDOUT = open(fn, "a")
        sys.stdout = NEW_STDOUT
    else:
        print >>sys.stderr, "NO REDIRECT"
        OLD_STDOUT = None
        NEW_STDOUT = None

def redirect_stdout_stop():
    global OLD_STDOUT, NEW_STDOUT
    if NEW_STDOUT:
        NEW_STDOUT.close()
    if OLD_STDOUT:
        sys.stdout = OLD_STDOUT

def log_message(lvl, msg, *args):
    if VERBOSE >= lvl:
        print "<%d> %s" % (lvl, APP), time.strftime("%Y-%m-%d %H:%M:%S -"), msg, ", ".join(map(str,args))
        sys.stdout.flush()