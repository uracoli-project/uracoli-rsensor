#!/usr/bin/env python
#   Copyright (c) 2014 - 2017 Axel Wachtler
#   All rights reserved.
#
#   Redistribution and use in source and binary forms, with or without
#   modification, are permitted provided that the following conditions
#   are met:
#
#   * Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the authors nor the names of its contributors
#     may be used to endorse or promote products derived from this software
#     without specific prior written permission.
#
#   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
#   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
#   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
#   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
#   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
#   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#   POSSIBILITY OF SUCH DAMAGE.

# $Id$
"""
Rsensor Flask Web Application.

Usage:

    python rsensor [OPTIONS]

Options:

    -C, --config <configfile>
        config file, (default: rsensor.cfg)
    -D, --database <dbname>
        location of the sqlite3 database file, which is created and updated
        by rsensor_logger.py.
        in configfile use:
            "dbname: <dbfile>"
    -F, --flaskdir <directory>
        This parameter lists a directory, that contains the "static" and
        "templates" sub-folders.
        in configfile use:
            "flaskdir: <directory>"
    -d, --debug
        enable debugging of web application (default: nodebug)
        in configfile use:
            "debug: True"
    -L, --logname <logfile>
        name of the logfile, default: /dev/null
        in configfile use:
            "logname: <logname>"
    -h, --help
        show help and exit
    -V, --version
        show version and exit
    -v, --verbose
        increase verbose level

Example Config File:

    [rsensor]
    # this section is used by rsensor_log_messageger and rsensor_server commonly
    # and overrides variables from the particular application section.
    dbname = my_rsensor.db

    [rsensor_server.py]
    debug = False
    verbose = 2
    #logname = /dev/null

    [rsensor_log_messageger.py]
    # see rsensor_log_messageger.py -h
    ...
"""

# === moduls ===================================================================
from flask import Flask, jsonify, render_template, request, Response, g
import rsensor as rs
import glob
import time
import sys
import os
import random
import math
import getopt
import sqlite3
import pprint
import traceback
pp = pprint.pprint
# === globals ==================================================================
APP_NAME = os.path.basename(sys.argv[0])
VERSION = rs.VERSION

# probe for flask dir
fdirs = [sys.exec_prefix + "/rsensor/",
         sys.exec_prefix + "/local/rsensor/",
         "."]
fdir = [f for f in fdirs if os.path.exists(f)][0]

DEFAULTS = {
          'debug'    : False,
          'verbose'  : 0,
          'flaskdir' : fdir,
          'logname'  : None,
          'configfile': 'rsensor.cfg'
}

DBFILE   = None
SENSOR_HREF = {
    "s4567_0001_00_temp" : "/bm",
    "s4567_0001_03_temp" : "/bm"
}

flask_app = Flask( __name__)
log_message = rs.log_message
pp = pprint.pprint

CFG = {}
# === functions ================================================================

def get_options(args, docstr):
    global DEFAULTS, APP_NAME, VERSION
    rv = {"configfile": DEFAULTS["configfile"]}

    opts, args = getopt.getopt(args, "hVD:dvF:C:l:",
                               ["help", "version", "database=", "debug",
                                "verbose", "flaskdir=", "configfile=",
                                "logname="])
    for o,v in opts:
        if o in ("-C", "--config"):
            rv["configfile"] = v
        elif o in  ("-D", "--database"):
            rv['dbname'] = v
        elif o in  ("-F", "--flaskdir"):
            rv['flaskdir'] = v
        elif o in  ("-l", "--logname"):
            print "set logname", v
            rv['logname'] = v
        elif o in ("-d", "--debug"):
            rv['debug'] = True
        elif o in ("-v", "--verbose"):
            if rv.has_key("verbose"):
                rv['verbose'] += 1
            else:
                rv['verbose'] = 1
        elif o in ("-h", "--help"):
            print docstr
            rv = None
            break
        elif o in ("-V", "--version"):
            print APP_NAME, VERSION
            rv = None
            break
    if rv != None:
        rv = rs.read_and_merge_cfg_file( rv.get("configfile"),
                                         ["rsensor_server.py", "rsensor"],
                                         DEFAULTS,
                                         rv )
    return rv

# todo: don't mess with globals here
def validate_params(cfg):
    global DBFILE, flask_app
    if not os.path.exists(cfg['dbname']):
        rs.exit_on_error(1, "DbFileNotExisting: %s" % cfg['dbname'])
    else:
        DBFILE = cfg['dbname']

    if not os.path.isdir(cfg['flaskdir']):
        rs.exit_on_error(1, "FlaskDirNotExisting: %s" % cfg['flaskdir'])
    else:
        flask_app.template_folder = os.path.join(cfg['flaskdir'], "templates")
        flask_app.static_folder = os.path.join(cfg['flaskdir'], "static")
        if not os.path.isdir(flask_app.template_folder):
            rs.exit_on_error(1, "TemplateDirNotExisting: %s" % flask_app.template_folder)
        if not os.path.isdir(flask_app.static_folder):
            rs.exit_on_error(1, "StaticDirNotExisting: %s" % flask_app.static_folder)

def flask_get_database_object():
    global CFG
    if not hasattr(g, "_db"):
        g._db = rs.db.db_open(**CFG)
        print "open db", g._db
    return g._db

# === flask routing ============================================================
@flask_app.teardown_appcontext
def flask_do_exit(exception):
    #if hasattr(g, '_db'):
    #    del(g._db)
    print "app exit"
###
### todo: this function urgently needs rewriting!!
###   apply dictionary query approach from:
###   http://stackoverflow.com/questions/3300464/how-can-i-get-dict-from-sqlite-query
###
@flask_app.route("/")
def index_page( title = "main page" ):
    print "** / **s"
    db = flask_get_database_object()
    nodes = db.query("select * from node;")
    tm_now = int(time.time())
    for n in nodes:
        last_seen = tm_now - n['last_update']
        n['last_seen'] = last_seen
        n['last_update'] = time.strftime("%Y-%m-%d %H:%M:%S")

        if last_seen > n['heart_beat'] * 5:
            n['status'] = "critical"
        elif last_seen > n['heart_beat'] * 2:
            n['status'] = "warning"

    sensors = db.query("select * from sensor, meas_series where meas_series.meas_id = sensor.meas_id;")
    return  render_template("index.html", title = title, nodes = nodes, sensors = sensors,  home = 1)



# some playing around
#@flask_app.route("/g1")
#def draw_live_graph():
#    return render_template("graph-live.html")

# more playing around
#@flask_app.route("/g2")
#def draw_time_graph():
#    return render_template("graph-time.html")

#@flask_app.route("/bm")
#def draw_bm():
#    d = int(request.args.get('d', 7*24))
#    tnow = time.time()
#    if d > 0:
#        tstart = tnow - d * 60 * 60
#    if tstart < 0:
#        tstart = 0
#    print "*d=", int(d), type(d), tstart
#    cur = get_db().cursor()
#    for f in ['s4567_0001_03_temp', 's4567_0001_00_temp']:
#        cur.execute("select count(name) from sqlite_master" \
#                    " where type='table' and name='%s';" % f)
#        print f, cur.fetchall()
#    cur.execute("select time,value from s4567_0001_03_temp where time > %d;" % tstart)
#    tdata = [[c[0]*1000.,c[1]] for c in cur.fetchall()]
#    cur.execute("select time,value from s4567_0001_00_temp where time > %d;" % tstart)
#    tluft = [[d[0]*1000.,d[1]] for d in cur.fetchall()]
#    return render_template("bm.html", title = "Biomeiler Temperature",
#                            tdata = tdata, tluft = tluft)

#@flask_app.route("/data.bm/<dur>")
#def get_bm_data(dur = 42):
#    print "data.bm:", dur
#    tnow = int(time.time())
#    values = {
#        "1d": 24 * 60 * 60,
#        "1w": 7 * 24 * 60 * 60,
#        "1m": 30 * 24 * 60 * 60,
#        "1y": 365 * 24 * 60 * 60,
#        "all": tnow,
#    }
#    tstart = tnow  - values.get(dur, tnow)
#    if tstart < 0:
#        tstart = 0
#    print "tstart/tnow/tdur:", tstart, tnow, values.get(dur, tnow)
#    cur = get_db().cursor()
#    cur.execute("select time,value from s4567_0001_03_temp where time >= %d;" % tstart)
#    tbio = [[c[0]*1000.,c[1]] for c in cur.fetchall()]
#    cur.execute("select time,value from s4567_0001_00_temp where time >= %d;" % tstart)
#    tair = [[d[0]*1000.,d[1]] for d in cur.fetchall()]
#    return jsonify(rv = [tbio, tair])

@flask_app.route("/data.json")
def get_time_data():
    print "data.json"
    fl = random.randrange(10) *100
    rv = [
            [[i, math.sin(i)] for i in range(1,fl+1)],
            [[i, math.sin(i)/i] for i in range(1,fl+1)],
            [[i, math.cos(i)] for i in range(1,fl+1)],
       ]
    return jsonify(rv = rv)

#@flask_app.route("/g")
#def draw_graph():
#    return render_template("graph.html")

@flask_app.route("/s/<sensorname>")
def draw_sensor(sensorname):
    db = flask_get_database_object()
    sdata = [[c['time']*1000., c['value']] for c in db.xquery("select time,value from %s;" % sensorname)]

    return render_template("graph.html", title = sensorname, sname = sensorname, sdata = sdata)

@flask_app.route("/t/<tbl_name>")
def table_sensor(tbl_name):
    db = flask_get_database_object()
    series_info = db.query("SELECT * from meas_series where tbl_name = '%s'" % tbl_name)[0]
    t = db.query("SELECT min(ts), max(ts) from %s" % tbl_name)[0]
    series_info.update(t)
    d = db.query("SELECT * from %s order by ts desc limit 20" % tbl_name)
    series_info["data"] = d
    return render_template("table.html", title = tbl_name, ms = series_info)


#MyCnt = 0
#@flask_app.route("/get_live_data")
#def get_live_data():
#    global MyCnt
#    import json
#    MyCnt += 1
#    rv = random.randrange(-10,10)
#    print "get_live_data", rv, request.args
#    return jsonify(p = [int(time.time())*1000,rv])# json.dumps(rv)

@flask_app.route("/<path:filename>")
def static_file(filename):
    fn = os.path.join(flask_app.static_folder(), filename)
    print fn, filename
    return flask_app.send_static_file(filename)

# === main =====================================================================
if __name__ in ("__main__", "rsensor_server"):
    cfg = get_options(sys.argv[1:], __doc__)
    if not cfg:
        sys.exit(0)
    rs.redirect_stdout_start(fn = cfg.get('logname'))
    rs.VERBOSE = cfg.get("verbose", 0)
    rs.APP = APP_NAME
    validate_params(cfg)
    CFG.update(cfg)

    log_message(0, APP_NAME + " version " + rs.VERSION)
    log_message(2, "config:\n", pprint.pformat(cfg))
    log_message(2, "flask statics:", flask_app.static_folder)
    log_message(2, "flask templates:", flask_app.template_folder)

    if __name__ == "__main__":
        flask_app.run(debug = cfg['debug'],
                      host = '0.0.0.0')
    #else: use uwsgi
