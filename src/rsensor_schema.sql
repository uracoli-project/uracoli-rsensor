/*
 * Schema for rsensor database
 */

CREATE TABLE IF NOT EXISTS `meas_series`
(
    `meas_id` INTEGER PRIMARY KEY,
    `deployed` TIMESTAMP DEFAULT current_timestamp,
    /* --- sensor meta data --- */
    `sens_name` VARCHAR(64) NOT NULL,
    `unit` VARCHAR(8) DEFAULT NULL,
    `constrained` VARCHAR(128) DEFAULT NULL,
    `data_type` VARCHAR(16) DEFAULT NULL,
    /* --- location meta data --- */
    `loc_name` VARCHAR(64) DEFAULT NULL,
    `geoloaction` VARCHAR(64) DEFAULT NULL,
    `description` VARCHAR(1024) DEFAULT NULL,
    /* --- hardware meta information  --- */
    `boardtype` VARCHAR(32) DEFAULT NULL, /* HW identification */
    `fw_version` VARCHAR(8) DEFAULT NULL,
    `hw_info` VARCHAR(1024) DEFAULT NULL, /* parameters of the sensor HW, e.g. channel */

    /* --- measurement series name, data table name --- */
    `tbl_name` VARCHAR(128) NOT NULL UNIQUE

);

CREATE TABLE IF NOT EXISTS `sensor`
(
    `sens_name` VARCHAR(64) NOT NULL PRIMARY KEY,
    `last_update` TIMESTAMP NOT NULL DEFAULT current_timestamp,
    `meas_id` INTEGER DEFAULT NULL,
    CONSTRAINT fk_meas_id FOREIGN KEY (meas_id) REFERENCES meas_series(meas_id)
);

CREATE TABLE IF NOT EXISTS `node`
(
    `addr` INTEGER PRIMARY KEY,
    `last_update` UNSIGNED INTEGER,
    `seq_number` TINYINT,
    `heart_beat` UNSIGNED INTEGER DEFAULT 300,
    `raw_data` VARCHAR(1024)
);

/* this tables will be created from entries in meas_series.tbl_name */
CREATE TABLE IF NOT EXISTS `meas_series_example`
(
    `ts` TIMESTAMP NOT NULL PRIMARY KEY,
    `val` REAL,
    `inv` BOOLEAN DEFAULT 0
);
