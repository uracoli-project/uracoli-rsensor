# === imports ==================================================================
from distutils.core import setup
import shutil
from rsensor import VERSION as pkg_version

# === globals ==================================================================
rsensor_data = [
        ('rsensor/templates', [
                                'templates/graph.html',
                                'templates/graph-live.html',
                                'templates/graph-time.html',
                                'templates/index.html',
                                'templates/layout.html',
                                'templates/bm.html']
        ),
        ('rsensor/static', [
                                'static/highcharts-logo.svg',
                                'static/rsensor.css',
                                'static/uracoli-logo.png'
                                ]
        ),
        ('rsensor/etc/init.d',         ['rsensor_logger',
                                        'rsensor_server'
                                ]),
        ('rsensor/etc',  ['rsensor.cfg',
                          'rsensor_uwsgi.ini',
                          'rsensor_nginx',
                          ]),
        ('rsensor/share',  ['rsensor_schema.sql',
                          ]),

        ('rsensor/doc',     ['README', 'LICENSE'])

    ]
with open("README") as r:
    ldesc = r.read()

setup(name = 'uracoli-rsensor',
      version = pkg_version,
      description = 'A framework for the uracoli rsensor firmware',
      long_description = ldesc,
      maintainer = "Axel Wachtler",
      maintainer_email = "axel@uracoli.de",
      packages = ['rsensor'],
      scripts = ['rsensor_logger.py', 'rsensor_server.py' ],
      data_files = rsensor_data,
      install_requires = ['pyserial==2.5', 'flask'],
      license = "BSD"
      )
