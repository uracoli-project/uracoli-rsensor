# Package handling

## Upload

    pip install twine
    twine register -r pypitest -u uracolix uracoli-rsensor-0.1.0.zip
    twine upload -r pypitest -u uracolix uracoli-rsensor-0.1.0.zip


Find login credentials in ~/.pypirc

    [distutils]
    index-servers = pypitest

    ##[pypi]
    ##repository=https://pypi.python.org/pypi
    ##username=uracolix
    ##password=<mypassword>

    [pypitest]
    repository=https://testpypi.python.org/pypi
    username=uracolix
    password=<mypassword>


## Pages

    + index.html
      |
      + usr_page1
      + usr_page2
      :
      |
      + [passwd] senor_overview
      + [passwd] config
                 (add and configure pages)

- usr-page definitions stored in DB

## Docker for non-root users

    sudo gpasswd -a ${USER} docker
    sudo service docker restart

see also: http://askubuntu.com/questions/477551/how-can-i-use-docker-without-sudo


## SQL Code

* SQL Style Guide
  http://www.sqlstyle.guide/

* Vergleich MySQL / SQLite
   http://db-engines.com/de/system/MySQL%3BSQLite